from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import StaleElementReferenceException
from helpers import load_yaml_locator


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.locator_type = "xpath"
        self.locators = load_yaml_locator()

    def open_page(self, base_url):
        self.driver.get(base_url)

    def get_element(self, locator, locator_type=None):
        locator_type = locator_type or self.locator_type
        return self.driver.find_element(locator_type, locator)

    def get_list_of_elements(self, locator, locator_type=None):
        locator_type = locator_type or self.locator_type
        return self.driver.find_elements(locator_type, locator)

    def click_on_element(self, locator):
        if self.is_element_clickable(locator):
            element = self.get_element(locator)
            element.click()
        else:
            pass

    def hover_mouse_on_object(self, locator):
        actions = ActionChains(self.driver)
        if self.is_element_visible(locator):
            element = self.get_element(locator)
            actions.move_to_element(element).perform()
        else:
            pass

    def scroll_to(self, direction):
        if direction == "down":
            self.driver.execute_script("window.scrollBy(0, 1500);")
        elif direction == "up":
            self.driver.execute_script("window.scrollBy(0, -1500);")

    def scroll_to_element(self, locator):
        if self.is_element_visible(locator):
            element = self.get_element(locator)
            self.driver.execute_script("arguments[0].scrollIntoView();", element)
        else:
            pass

    def is_element_exist(self, locator):
        try:
            self.wait_for_presence(locator)
            return True
        except TimeoutException:
            return False

    def is_element_clickable(self, locator):
        try:
            self.wait_to_click(locator)
            return True
        except TimeoutException:
            return False

    def is_element_visible(self, locator, locator_type=None):
        locator_type = locator_type or self.locator_type
        try:
            wait = WebDriverWait(self.driver, 5)
            wait.until(ec.visibility_of_element_located((locator_type, locator)))
            return True
        except StaleElementReferenceException:
            return False

    def is_iframe_active(self, locator, locator_type=None):
        locator_type = locator_type or self.locator_type
        try:
            wait = WebDriverWait(self.driver, 3)
            # TODO add self.timeout and re-work all occurrences
            wait.until(ec.visibility_of_element_located((locator_type, locator)))
            return True
        except TimeoutException:
            return False

    def switch_to_iframe(self, locator, locator_type):
        if self.is_iframe_active(locator, locator_type):
            self.driver.switch_to.frame(locator)
        else:
            pass

    def switch_to_default(self):
        self.driver.switch_to.default_content()

    def wait_for_presence(self, locator):
        wait = WebDriverWait(self.driver, 10)
        wait.until(ec.presence_of_element_located((By.XPATH, locator)))

    def wait_to_click(self, locator):
        wait = WebDriverWait(self.driver, 10)
        wait.until(ec.element_to_be_clickable((By.XPATH, locator)))
