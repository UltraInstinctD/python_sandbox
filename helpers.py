import yaml


def load_yaml_locator():
    with open("locators.yml", "r", encoding="utf-8") as yml:
        locator_dict = yaml.safe_load(yml)
        return locator_dict["mvideo_locators"]
