import time
from base_page import BasePage


class MarketPageClass(BasePage):

    def start(self):
        self.open_page("https://www.mvideo.ru/")

    def notebooks_page(self):
        self.click_on_element(self.locators["registration_close_locator"])
        time.sleep(1)
        self.click_on_element(self.locators["computers"])
        self.click_on_element(self.locators["notebooks"])

    def i7_checkbox_click(self):
        self.scroll_to_element(self.locators["diagonal_locator"])
        self.click_on_element(self.locators["i7_checkbox"])

    def frame_interaction(self):
        if self.is_iframe_active(self.locators["iframe"], "id"):
            self.switch_to_iframe(self.locators["iframe"], "id")
            self.click_on_element(self.locators["iframe_close_locator"])
            self.switch_to_default()
        else:
            pass

    def price_sorting(self):
        if self.is_element_clickable(self.locators["sorter_list_button"]):
            self.click_on_element(self.locators["sorter_list_button"])
            self.click_on_element(self.locators["filter_list_button"])
        self.click_on_element(self.locators["descending_price_filter"])
        time.sleep(0)

    def get_price_list(self):
        elements_list = self.get_list_of_elements(self.locators["price_list_locator"])
        price_list = []
        for element in elements_list:
            price_list.append(element.text)
        return price_list

    def descending_price_check(self):
        price_list = self.get_price_list()
        price_sorted = sorted(price_list, reverse=True)
        return price_sorted == price_list

    def method_list(self):
        self.start()
        self.notebooks_page()
        self.i7_checkbox_click()
        self.frame_interaction()
        self.price_sorting()
        self.get_price_list()
