import pytest
import os
from selenium import webdriver
from datetime import datetime


def pytest_addoption(parser):
    parser.addoption("--browser")


@pytest.fixture(scope="module")
def driver(request):
    browser = request.config.getoption("--browser")
    if browser == "firefox":
        _browser_profile = webdriver.FirefoxProfile()
        _browser_profile.set_preference("dom.webnotifications.enabled", False)
        driver = webdriver.Firefox(firefox_profile=_browser_profile)
    elif browser == "chrome":
        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)
        driver = webdriver.Chrome(chrome_options=chrome_options)
    else:
        raise AttributeError("browser param :: value should be set inside pytest configuration")
    driver.maximize_window()
    driver.implicitly_wait(3)
    failed_before = request.session.testsfailed

    def teardown():
        if request.session.testsfailed != failed_before:
            take_screenshot(driver, request)
        driver.quit()
    request.addfinalizer(teardown)
    return driver


def take_screenshot(driver, request):
    test_name = request.node.nodeid.split('::')[-1]
    time_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    file_name = f'{test_name}__{time_now}.png'
    project_dir_path = os.path.dirname(__file__)
    screenshot_dir = os.path.realpath(os.path.join(project_dir_path, "screenshots", file_name))
    driver.save_screenshot(screenshot_dir)
